/*
create database sae_Diego
on primary 
(name= N'sae_Diego', filename = N'D:\Databases\sae_Diego.mdf')
log on (name = N'sae_Diego_log', filename = N'D:\Databases\sae_Diego.log')

-- Tengo que definir los archivos en el disco D: por problemas de espacio en el C: de la m�quina... 

-- A continuaci�n pego script del Curso
*/
USE sae_Diego; -- Cambio a DB correspondiente 

/*
-- comisiones tiene 3
alter table comisiones
drop constraint FK_COM_MAT

alter table comisiones
drop constraint FK_COM_CUR

alter table comisiones
drop constraint FK_COM_PROF;

-- materias tiene 1
alter table materias
drop constraint FK_MAT_CUR ;

-- partidos tiene 1
alter table partidos
drop constraint FK_PART_PROV;

-- alunnos tiene 2
alter table alumnos
drop constraint FK_ALU_PART;

alter table alumnos
drop constraint FK_ALU_COM;

--profesores tiene 1
alter table profesores
drop constraint FK_PROF_PART;

-- modulos tiene 1
alter table modulos
drop constraint FK_MOD_MAT;
*/

-- Coment� lo anterior debido a que al no existir en primera instancia las tablas arroja error 
/*

DROP TABLE IF EXISTS  cursos;
CREATE TABLE cursos (
  CUR_ID INTEGER NOT NULL PRIMARY KEY(CUR_ID) IDENTITY,
  CUR_ANIO INTEGER,
  CUR_DESCRIPCION VARCHAR(50) 
 ); 


DROP TABLE IF EXISTS materias;

 CREATE TABLE materias (
  MAT_ID INTEGER NOT NULL PRIMARY KEY(MAT_ID) IDENTITY,
  CUR_ID INTEGER NOT NULL CONSTRAINT FK_MAT_CUR FOREIGN KEY (CUR_ID) REFERENCES cursos (CUR_ID),
  MAT_NOMBRE varchar(50) NOT NULL
 ) ;

drop table IF EXISTS provincias;
CREATE TABLE provincias(
  PROV_ID INTEGER NOT NULL PRIMARY KEY(PROV_ID) IDENTITY,
  PROV_NOMBRE VARCHAR(200)
  );

drop table IF EXISTS partidos;
CREATE TABLE partidos(
  PART_ID INTEGER NOT NULL PRIMARY KEY(PART_ID) IDENTITY,
  PROV_ID INTEGER NOT NULL CONSTRAINT FK_PART_PROV FOREIGN KEY(PROV_ID) REFERENCES provincias(PROV_ID),
  PART_NOMBRE varchar(50) NOT NULL
 );

DROP TABLE IF EXISTS profesores;
CREATE TABLE profesores (
  PROF_ID INTEGER NOT NULL PRIMARY KEY(PROF_ID) IDENTITY,
  PART_ID INTEGER NOT NULL CONSTRAINT FK_PROF_PART FOREIGN KEY (PART_ID) REFERENCES partidos(PART_ID),
  PROF_NOMBRE varchar(45) NOT NULL,
  PROF_APELLIDO varchar(45) NOT NULL,
  PROF_DIRECCION varchar(45) DEFAULT NULL,
  PROF_TELEFONO  varchar(45) DEFAULT NULL,
  PROF_DNI varchar(45) DEFAULT NULL,
  PROF_EMAIL varchar(45) DEFAULT NULL
  ) ;

DROP TABLE IF EXISTS comisiones;
CREATE TABLE comisiones (
  COM_ID INTEGER NOT NULL PRIMARY KEY(COM_ID) IDENTITY,
  PROF_ID INTEGER NOT NULL CONSTRAINT FK_COM_PROF FOREIGN KEY (PROF_ID) REFERENCES profesores(PROF_ID),
  CUR_ID INTEGER NOT NULL CONSTRAINT FK_COM_CUR FOREIGN KEY (CUR_ID) REFERENCES cursos(CUR_ID),
  MAT_ID INTEGER NOT NULL CONSTRAINT FK_COM_MAT FOREIGN KEY (MAT_ID) REFERENCES materias(MAT_ID),
  COM_TURNO varchar(45) NOT NULL,
  COM_DIVISION varchar(45) NOT NULL  
) ;

drop table IF EXISTS alumnos;
CREATE TABLE alumnos(
	ALU_ID INTEGER NOT NULL PRIMARY KEY(ALU_ID) IDENTITY,
	PART_ID INTEGER NOT NULL CONSTRAINT FK_ALU_PART FOREIGN KEY (PART_ID) REFERENCES partidos(PART_ID),
	COM_ID INTEGER NOT NULL CONSTRAINT FK_ALU_COM FOREIGN KEY (COM_ID) REFERENCES comisiones(COM_ID),
	ALU_NOMBRE VARCHAR(50), 
	ALU_APELLIDO VARCHAR(50),
	ALU_DNI VARCHAR(50),
	ALU_TELEFONO VARCHAR(50),
	ALU_EMAIL VARCHAR(50)
);

DROP TABLE IF EXISTS modulos;
CREATE TABLE modulos (
  MOD_ID INTEGER NOT NULL PRIMARY KEY(MOD_ID) IDENTITY,
  MAT_ID INTEGER NOT NULL CONSTRAINT FK_MOD_MAT FOREIGN KEY (MAT_ID) REFERENCES materias(MAT_ID),
  MOD_DESCRIPCION varchar(45) NOT NULL
 )

-- Hasta aqu� primer script proporcionado por el instructor 
*/
-- 2do script del curso:

USE sae_Diego; -- Modificado a lo que corresponde por alumno
-- use sae;  -- Instrucci�n duplicada
INSERT INTO cursos(CUR_ANIO,CUR_DESCRIPCION) VALUES 
 (1,'Primer a�o Avi�nica'),
 (2,'Segundo a�o Avi�nica'),
 (3,'Terecer a�o Avi�nica'),
 (4,'Cuarto a�o avi�nica'),
 (6,'Sexto a�o Avi�nica'),
 (7,'Septimo a�o Avi�nica'),
 (8,'Aeronavegantes'),
 (3,'Terecer a�o Avi�nica'),
 (1,'Primer a�o Mecanica'),
 (2,'Segundo a�o mec'),
 (5,'Quinto A�o Avi�nica');



INSERT INTO materias(CUR_ID,MAT_NOMBRE) VALUES 
 (4,'Teor�a de circuitos'),
 (11,'T�cnicas Digitales'),
 (11,'Electr�nica 5to');

 INSERT INTO provincias(PROV_NOMBRE) VALUES 
 ('Buenos Aires'),
 ('Catamarca'),
 ('Chaco'),
 ('Chubut'),
 ('C�rdoba'),
 ('Corrientes'),
 ('Entre R�os'),
 ('Formosa'),
 ('Jujuy'),
 ('La Pampa'),
 ('La Rioja'),
 ('Mendoza'),
 ('Misiones'),
 ('Neuqu�n'),
 ('R�o Negro'),
 ('Salta'),
 ('San Juan'),
 ('San Luis'),
 ('Santa Cruz'),
 ('Santa Fe'),
 ('Santiago del Estero'),
 ('Tierra del Fuego, Ant�rtida e Islas del Atl�ntico Sur'),
 ('Tucum�n');


INSERT INTO partidos (PROV_ID,PART_NOMBRE) VALUES 
 (1,'Adolfo Alsina'),
 (1,'Adolfo Gonzales Chaves'),
 (1,'Alberti'),
 (1,'Almirante Brown'),
 (1,'Arrecifes'),
 (1,'Avellaneda'),
 (1,'Ayacucho'),
 (1,'Azul'),
 (1,'Bah�a Blanca'),
 (1,'Balcarce'),
 (1,'Baradero'),
 (1,'Benito Ju�rez'),
 (1,'Berazategui'),
 (1,'Berisso'),
 (1,'Bol�var'),
 (1,'Bragado'),
 (1,'Brandsen'),
 (1,'Campana'),
 (1,'Ca�uelas'),
 (1,'Capit�n Sarmiento Carlos'),
 (1,'Carlos Casares'),
 (1,'Carlos Tejedor'),
 (1,'Carmen de Areco'),
 (1,'Castelli'),
 (1,'Chacabuco'),
 (1,'Chascom�s'),
 (1,'Chivilcoy'),
 (1,'Col�n'),
 (1,'Coronel de Marina Leonardo Rosales'),
 (1,'Coronel Dorrego'),
 (1,'Coronel Pringles'),
 (1,'Coronel Su�rez'),
 (1,'Daireaux'),
 (1,'Dolores'),
 (1,'Ensenada'),
 (1,'Escobar'),
 (1,'Esteban Echeverr�a'),
 (1,'Exaltaci�n de la Cruz'),
 (1,'Ezeiza'),
 (1,'Florencio Varela'),
 (1,'Florentino Ameghino'),
 (1,'General Alvarado'),
 (1,'General Alvear'),
 (1,'General Arenales'),
 (1,'General Belgrano'),
 (1,'General Guido'),
 (1,'General Juan Madariaga'),
 (1,'General La Madrid'),
 (1,'General Las Heras'),
 (1,'General Lavalle'),
 (1,'General Paz'),
 (1,'General Pinto'),
 (1,'General Pueyrred�n'),
 (1,'General Rodr�guez'),
 (1,'General San Mart�n'),
 (1,'General Viamonte'),
 (1,'General Villegas'),
 (1,'Guamin�'),
 (1,'Hip�lito Yrigoyen'),
 (1,'Hurlingham'),
 (1,'Ituzaing�'),
 (1,'Jos� C. Paz'),
 (1,'Jun�n'),
 (1,'La Costa'),
 (1,'La Matanza'),
 (1,'Lan�s'),
 (1,'La Plata'),
 (1,'Laprida'),
 (1,'Las Flores'),
 (1,'Leandro N. Alem'),
 (1,'Lincoln'),
 (1,'Lober�a'),
 (1,'Lobos'),
 (1,'Lomas de Zamora'),
 (1,'Luj�n'),
 (1,'Magdalena'),
 (1,'Maip�'),
 (1,'Malvinas Argentinas'),
 (1,'Mar Chiquita'),
 (1,'Marcos Paz'),
 (1,'Mercedez'),
 (1,'Merlo'),
 (1,'Monte'),
 (1,'Monte Hermoso'),
 (1,'Moreno'),
 (1,'Mor�n'),
 (1,'Navarro'),
 (1,'Necochea'),
 (1,'Nueve de Julio (9 de Julio)'),
 (1,'Olavarr�a'),
 (1,'Patagones'),
 (1,'Pehuaj�'),
 (1,'Pellegrini'),
 (1,'Pergamino'),
 (1,'Pila'),
 (1,'Pilar'),
 (1,'Pinamar'),
 (1,'Presidente Per�n'),
 (1,'Puan'),
 (1,'Punta Indio'),
 (1,'Quilmes'),
 (1,'Ramallo'),
 (1,'Rauch'),
 (1,'Rivadavia'),
 (1,'Rojas'),
 (1,'Roque P�rez'),
 (1,'Saavedra'),
 (1,'Saladillo'),
 (1,'Salliquel�'),
 (1,'Salto'),
 (1,'San Andr�s de Giles'),
 (1,'San Antonio de Areco'),
 (1,'San Cayetano'),
 (1,'San Fernando'),
 (1,'San Isidro'),
 (1,'San Miguel'),
 (1,'San Nicol�s'),
 (1,'San Pedro'),
 (1,'San Vicente'),
 (1,'Suipacha'),
 (1,'Tandil'),
 (1,'Tapalqu�'),
 (1,'Tigre'),
 (1,'Tordillo'),
 (1,'Tornquist'),
 (1,'Trenque Lauquen'),
 (1,'Tres Arroyos'),
 (1,'Tres de Febrero'),
 (1,'Tres Lomas'),
 (1,'Veinticinco de Mayo (25 de Mayo)'),
 (1,'Vicente L�pez'),
 (1,'Villa Gesell'),
 (1,'Villarino'),
 (1,'Z�rate');

INSERT INTO profesores(PART_ID,PROF_NOMBRE,PROF_APELLIDO,PROF_DIRECCION,PROF_TELEFONO,PROF_DNI,PROF_EMAIL) VALUES 
 (20,'Gabriel','Casas','Solari 3866','15-6005-7247','67965345','gcasas@gmail.com'),
 (21,'Juan','Perez','Sordeaux 3866','4697-1010','34965345','jperez@gmail.com'),
 (22,'Pedro','Luna','Santo Domingo 3866','4697-1011','22964545','pluna@gmail.com'),
 (23,'Roberto','Lippa','Cucha chucha 3866','4697-1012','18965345','rlippa@gmail.com'),
 (24,'Maria','Carniglia','Pierrestegui 3866','4697-1013','45965345','mcarniglia@gmail.com'),
 (25,'Jose','Lopez','San Martin 3866','4697-1014','22335345','jlopez@gmail.com'),
 (26,'Carlos','Ramirez','Belgrano 3866','4697-1015','24965345','cramirez@gmail.com'),
 (27,'Lorena','Miranda','Azul 3866','4697-1016','25965345','lmiranda@gmail.com'),
 (28,'Sabrina','Circovich','Verde 3866','4697-1017','27965345','ssircovich@gmail.com'),
 (29,'Joel','Antonov','Medrano 3866','4697-1018','23965345','gcasas@gmail.com'),
 (30,'Federico ','Weber','Medrano 3866','4697-1018','23965345','gcasas@gmail.com');
INSERT INTO comisiones (PROF_ID,CUR_ID,MAT_ID,COM_TURNO,COM_DIVISION) VALUES 
 (1,4,1,'Tarde','B'),  --com_id =1
 (2,4,1,'Tarde','A'),	--com_id =2
 (5,5,2,'Tarde','A'),  --com_id =3
 (8,5,2,'Tarde','B'),  --com_id =4
 (11,5,3,'Tarde','A');	--com_id =5

INSERT INTO alumnos(PART_ID,COM_ID,ALU_NOMBRE,ALU_APELLIDO,ALU_TELEFONO,ALU_DNI,ALU_EMAIL) VALUES 
 (90,5,'Esteban','Saavedra','su casa',NULL,NULL),
 (90,5,'Ezequiel','Barbagallo','su casa',NULL,NULL),
 (90,5,'Ivan','Abaca','su casa',NULL,NULL),
 (90,5,'Lucas','Alaimo','su casa',NULL,NULL),
 (90,5,'Lautaro','Jaimes','su casa',NULL,NULL),
 (90,5,'Karen','Maciel','su casa',NULL,NULL);







/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
INSERT INTO modulos (MAT_ID,MOD_DESCRIPCION) VALUES 
 (1,'Primer trimestre'),
 (1,'Segundo trimestre'),
 (1,'Tercer trimestre'),
 (1,'Integradora'),
 (1,'Diciembre'),
 (1,'Marzo'),
 (2,'Primer trimestre'),
 (2,'Segundo trimestre'),
 (2,'Tercer trimestre'),
 (2,'Integradora'),
 (2,'Diciembre'),
 (2,'Marzo'),
 (3,'Primer trimestre'),
 (3,'Segundo trimestre'),
 (3,'Tercer trimestre'),
 (3,'Diciembre'),
 (3,'Marzo');

 -- Hasta aqu� segundo script proporcionado por ejercicio
  
  -- Comienza 3er script:

  USE sae_Diego; -- Mod

SELECT * FROM ALUMNOS
SELECT * FROM PROFESORES
SELECT * FROM provincias
SELECT * FROM partidos

SELECT * FROM materias

SELECT * FROM CURSOS
SELECT * FROM COMISIONES
SELECT * FROM MODULOS


-- 1er Paso

use sae_Diego;

BACKUP DATABASE sae_Diego
	TO DISK ='D:\Databases\Backup\sae_Diego.bak'
	WITH INIT, COMPRESSION;

-- 

select name, database_id, recovery_model_desc as model
from sys.databases
where name ='sae_Diego'

select DATABASEPROPERTYEX('master', 'recovery')

select name, database_id, recovery_model_desc as model
from sys.databases
where name ='master'

/*
name	database_id	model
sae_Diego	9	FULL

SIMPLE

name	database_id	model
master	1	SIMPLE

*/

-- Luego se podra realizar un restore utilizando el siguiente comando

RESTORE DATABASE sae_Diego
  FROM DISK = 'D:\Databases\Backup\sae_Diego.bak'
  WITH REPLACE


  -- Fin